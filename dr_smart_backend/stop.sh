rm nohup.out
ps -ef | grep restart.sh | grep -v grep | awk '{print $2}' | xargs kill 9
ps -ef | grep Thrones | grep -v grep | awk '{print $2}' | xargs kill 9